package usersdirectory;

import org.springframework.data.repository.CrudRepository;

interface UserRepository extends CrudRepository<User, Long> {

    User findUserByUsername(String username);

}
