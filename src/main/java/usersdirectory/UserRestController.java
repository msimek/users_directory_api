package usersdirectory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/users", produces = "application/json")
@CrossOrigin(origins = "http://localhost:8080")
public class UserRestController {

    @Autowired
    UserService userService;

    @GetMapping
    @ResponseBody
    public List<User> getAllUsers() {
                return StreamSupport.stream(userService.getAllUsers().spliterator(), false)
                        .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}")
    @ResponseBody
    public User getUserById(@PathVariable(value = "id") Long id) {
        return userService.getUserById(id);
    }

    @PostMapping
    @ResponseBody
    public User addUser(@RequestBody Map<String, String> json) {
        return userService.addUser(json.get("name"), json.get("username"), json.get("password"));
    }

    @PatchMapping(value = "/{id}")
    @ResponseBody
    public User updateUser(@PathVariable(value = "id") Long id,
                           @RequestBody Map<String, String> json) {

        if (json.containsKey("name")) {
            userService.changeName(id, json.get("name"));

        }

        if (json.containsKey("username")) {
            userService.changeUsername(id, json.get("username"));
        }

        if (json.containsKey("password")) {
            userService.changePassword(id, json.get("password"));
        }

        return userService.getUserById(id);
    }



}
