package usersdirectory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/users/secured")
public class UserSecuredRestController {

    @Autowired
    UserService userService;

    @DeleteMapping("/{id}")
    @ResponseBody
    public void deleteUserById(@PathVariable(value = "id") Long id) {
        userService.removeUser(id);
    }
}
