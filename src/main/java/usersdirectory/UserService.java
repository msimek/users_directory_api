package usersdirectory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Repository
class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Transactional
    public User addUser(String name, String username, String password) {
        User user = new User(name, username, password);
        return userRepository.save(user);
    }

    @Transactional
    public void removeUser(Long id) {
        userRepository.delete(id);
    }

    @Transactional
    public void changeName(Long id, String name) {
        userRepository.findOne(id).setName(name);
    }

    @Transactional
    public void changeUsername(Long id, String username) {
        userRepository.findOne(id).setUsername(username);
    }

    @Transactional
    public void changePassword(Long id, String password) { userRepository.findOne(id).setPassword(password); }

}
